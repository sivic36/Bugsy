package hr.ferit.slavenivic.bugsy;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.slavenivic.bugsy.adapter.RSSClickCallback;
import hr.ferit.slavenivic.bugsy.adapter.RSSFeedAdapter;
import hr.ferit.slavenivic.bugsy.model.Item;
import hr.ferit.slavenivic.bugsy.model.RSS;
import hr.ferit.slavenivic.bugsy.networking.RSSFeedAPI;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class FeedActivity extends AppCompatActivity implements Callback<RSS> {

    private static final String BUG_API = "http://www.bug.hr/";
    RSSFeedAdapter feedAdapter;
    String listCategory[];

    // Callback for clicking list item - RSS and opening its link in internal browser
    RSSClickCallback clickCallback = new RSSClickCallback() {
        @Override
        public void onClick(Item item) {
            Intent intent = new Intent(FeedActivity.this, WebActivity.class);
            intent.putExtra(WebActivity.URL_WEB_VIEW, item.getLink());
            startActivity(intent);
        }
    };

    @BindView(R.id.rv_feed) RecyclerView rvFeed;
    @BindView(R.id.swipeContainer) SwipeRefreshLayout swipeContainer;
    @BindView(R.id.spinner_ctg) MaterialSpinner spinnerCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);
        ButterKnife.bind(this);

        listCategory = getResources().getStringArray(R.array.categories);
        spinnerCategory.setItems(listCategory);
        setupRV();
        setupSwipeToRefresh();
        getRSSFeed();
    }

    private void setupRV(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
        DividerItemDecoration divider = new DividerItemDecoration(
                this, layoutManager.getOrientation());
        feedAdapter = new RSSFeedAdapter(this, clickCallback);

        rvFeed.setLayoutManager(layoutManager);
        rvFeed.addItemDecoration(divider);
        rvFeed.setAdapter(feedAdapter);
    }

    private void setupSwipeToRefresh(){
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRSSFeed();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    // Method for getting the newest feed from bug.hr/rss
    private void getRSSFeed() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BUG_API)
                .client(new OkHttpClient())
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        RSSFeedAPI api = retrofit.create(RSSFeedAPI.class);
        // Getting the category from spinner and getting the RSSFeed
        Call<RSS> call = api.getRSSFeed(listCategory[spinnerCategory.getSelectedIndex()].toLowerCase());
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<RSS> call, Response<RSS> response) {
        if(response.body() != null){
            List<Item> list = response.body().getChannel().getList();
            feedAdapter.refreshFeed(list);
        }
        else Toast.makeText(getApplicationContext(),
                "No results!", Toast.LENGTH_LONG).show();
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onFailure(Call<RSS> call, Throwable t) {
        Log.i("Fail", t.getMessage());
    }
}
