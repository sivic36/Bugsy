package hr.ferit.slavenivic.bugsy.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Ivic on 4/27/2018.
 */

@Root(strict = false)
public class RSS {

    @Element(name = "channel")
    private Channel channel;

    public Channel getChannel() {
        return channel;
    }
}
