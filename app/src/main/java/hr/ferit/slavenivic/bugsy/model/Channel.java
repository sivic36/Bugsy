package hr.ferit.slavenivic.bugsy.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Ivic on 4/28/2018.
 */

@Root(strict = false)
public class Channel {

    @ElementList(name = "item", inline = true)
    private List<Item> list;

    public List<Item> getList() {
        return list;
    }

}