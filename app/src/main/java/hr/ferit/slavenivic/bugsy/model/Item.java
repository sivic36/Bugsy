package hr.ferit.slavenivic.bugsy.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Ivic on 4/27/2018.
 */

@Root(strict = false)
public class Item {
    @Element(name = "pubDate") private String pubDate;
    @Element(name = "category") private String category;
    @Element(name = "title") private String title;
    @Element(name = "enclosure" , required = false) private Enclosure image;
    @Element(name = "description") private String description;
    @Element(name = "link") private String link;

    public String getPubDate() {
        return pubDate;
    }
    public String getCategory() {
        return category;
    }
    public String getTitle() {
        return title;
    }
    public String getDescription() {
        return description;
    }
    public String getLink() {
        return link;
    }
    public Enclosure getImage() {
        return image;
    }
}
