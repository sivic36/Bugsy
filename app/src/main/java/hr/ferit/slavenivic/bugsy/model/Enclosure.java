package hr.ferit.slavenivic.bugsy.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Ivic on 4/28/2018.
 */

@Root(name = "enclosure", strict = false)
public class Enclosure {

    @Attribute(name = "url", required = false)
    private String Url;

    public String getUrl() {
        return Url;
    }
}
