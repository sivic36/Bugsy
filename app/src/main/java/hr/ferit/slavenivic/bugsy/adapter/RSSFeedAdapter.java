package hr.ferit.slavenivic.bugsy.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.slavenivic.bugsy.R;
import hr.ferit.slavenivic.bugsy.model.Item;

/**
 * Created by Ivic on 4/28/2018.
 */

public class RSSFeedAdapter extends RecyclerView.Adapter<RSSFeedAdapter.ViewHolder> {

    private List<Item> rssFeed;
    Context context;
    private RSSClickCallback clickCallback;

    public RSSFeedAdapter(Context context, RSSClickCallback clickCallback) {
        this.clickCallback = clickCallback;
        this.context = context;
    }

    @Override
    public RSSFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View feedView = inflater.inflate(R.layout.item_view, parent, false);
        ViewHolder feedViewHolder = new ViewHolder(feedView, clickCallback);
        return feedViewHolder;
    }

    @Override
    public void onBindViewHolder(RSSFeedAdapter.ViewHolder holder, int position) {
        Item rssItem = this.rssFeed.get(position);
        holder.title.setText(rssItem.getTitle());
        holder.description.setText(rssItem.getDescription());
        holder.category.setText(rssItem.getCategory());
        holder.date.setText(rssItem.getPubDate().substring(0, rssItem.getPubDate().length()-5));
        Picasso.get().load(rssItem.getImage().getUrl()).into(holder.image);
    }

    public void refreshFeed(List<Item> newRssFeed){
        this.rssFeed = newRssFeed;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(rssFeed!=null)
            return this.rssFeed.size();
        else
            return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title) TextView title;
        @BindView(R.id.description) TextView description;
        @BindView(R.id.category) TextView category;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.date) TextView date;

        ViewHolder(View itemView, final RSSClickCallback clickCallback) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickCallback.onClick(rssFeed.get(getAdapterPosition()));
                }
            });
        }
    }
}

