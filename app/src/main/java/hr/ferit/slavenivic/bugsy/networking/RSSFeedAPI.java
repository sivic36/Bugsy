package hr.ferit.slavenivic.bugsy.networking;

import hr.ferit.slavenivic.bugsy.model.RSS;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Ivic on 4/27/2018.
 */

public interface RSSFeedAPI {
    @GET("rss/{category}")
    // Added 'path variable' which represents RSS category
    Call<RSS> getRSSFeed(@Path("category") String category);
}
