package hr.ferit.slavenivic.bugsy.adapter;

import hr.ferit.slavenivic.bugsy.model.Item;

/**
 * Created by Ivic on 4/29/2018.
 */

public interface RSSClickCallback {
    void onClick(Item item);
}
