# RMA Homework project

## LV4 - Bugsy

**Bugsy** - RSS Feed Android app

<img src="screenshots/logo.png" title="Bugsy logo" width="100" height="100">

### The assignment and problems encountered

App shows RSS Feed from [Bug.hr](http://www.bug.hr) - image, title, short description and allows user to read whole news on http://bug.hr by clicking on RSS item of the list. The list can be refreshed with `Pull-To-Refresh` and user can filter out the RSS Feed by selecting one of the categories from spinner.

### Utilised snippets/solutions/libraries

* RecyclerView

```gradle
implementation 'com.android.support:recyclerview-v7:26.1.0'
```

* **Retrofit**

```gradle
implementation 'com.squareup.retrofit2:retrofit:2.4.0'
```

* **Simple XML**

 ```gradle
implementation ('com.squareup.retrofit2:converter-simplexml:2.4.0') {
        exclude group: 'xpp3', module: 'xpp3'
        exclude group: 'stax', module: 'stax-api'
        exclude group: 'stax', module: 'stax'
    }
```

* ButterKnife

```gradle
implementation 'com.jakewharton:butterknife:8.8.1'
annotationProcessor 'com.jakewharton:butterknife-compiler:8.8.1'
```

* Material Spinner - stylized Material Design spinner

 ```gradle
implementation 'com.jaredrummler:material-spinner:1.2.5'
```

* Picasso

 ```gradle
implementation 'com.squareup.picasso:picasso:2.71828'
```

### Screenshots

<img src="screenshots/screen1.png" title="FeedActivity" height="350">
<img src="screenshots/screen2.png" title="Selecting category" height="350">
<img src="screenshots/screen3.png" title="Refreshing feed" height="350">
<img src="screenshots/screen4.png" title="Refreshed feed" height="350">
<img src="screenshots/screen5.png" title="Web View inside app" height="350">

### Key points

* **Getting the data**

The XML response from [Bug.hr](http://www.bug.hr/rss) is in the following format:

```XML
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0">
    <channel>
        <title>bug.hr</title>
        <description>Sve objave na bug.hr</description>
        <language>hr</language>
        <copyright>Copyright 2003-2018 Bug d.o.o. &amp; dizzy.hr</copyright>
        <image>
            <url>http://www.bug.hr/Assets/images/rss.png</url>
            <link>http://www.bug.hr</link>
            <title>bug.hr</title>
        </image>
        <ttl>30</ttl>
        <link>http://www.bug.hr</link>
        <item>
            <guid isPermaLink="true">http://www.bug.hr/sigurnost/startup-tvrtka-se-specijalizirala-za-kupnju-i-preprodaju-propusta-3985
            </guid>
            <link>http://www.bug.hr/sigurnost/startup-tvrtka-se-specijalizirala-za-kupnju-i-preprodaju-propusta-3985
            </link>
            <category>Sigurnost</category>
            <title>Startup tvrtka se specijalizirala za kupnju i preprodaju propusta </title>
            <description>Crowdfense, startup tvrtka iz Ujedinjenih Arapskih Emirata bavi se kupnjom exploita za Windows, iOS, Androidi macOS uređaje koje onda preprodaje zainteresiranim kupcima</description>
            <pubDate>Sat, 28 Apr 2018 08:00:00 Z</pubDate>
            <enclosure length="12345" type="image/jpeg" url="http://www.bug.hr/img/startup-tvrtka-se-specijalizirala-za-kupnju-i-preprodaju-propusta_a4Pvnb.jpg" />
        </item>
        <item>
```

By looking at the XML structure the following POJO classes for reading the data is created to match the XML sheme:

```java
@Root(strict = false)
public class RSS {
    @Element(name = "channel")
    private Channel channel;

    public Channel getChannel() {return channel;}
}
```

```java
@Root(strict = false)
public class Channel {
    @ElementList(name = "item", inline = true)
    private List<Item> list;

    public List<Item> getList() {return list;}
}
```

```java
@Root(strict = false)
public class Item {
    @Element(name = "pubDate") private String pubDate;
    @Element(name = "category") private String category;
    @Element(name = "title") private String title;
    @Element(name = "enclosure" , required = false) private Enclosure image;
    @Element(name = "description") private String description;
    @Element(name = "link") private String link;

    public String getPubDate() {return pubDate;}    
    public String getCategory() {return category;}
    public String getTitle() {return title;}
    public String getDescription() {return description;}
    public String getLink() {return link;}
    public Enclosure getImage() {return image;}
}
```

```java
@Root(name = "enclosure", strict = false)
public class Enclosure {
    @Attribute(name = "url", required = false)
    private String Url;

    public String getUrl() {return Url;}
}
```

`Retrofit` and `GET` method are used to fetch the data (RSS Feed) using getRSSFeed method which takes a *category* string as an input argument, representing a `@Path` variable:

```java
public interface RSSFeedAPI {
    @GET("rss/{category}")
    // Added 'path variable' which represents RSS category
    Call<RSS> getRSSFeed(@Path("category") String category);
}
```

FeedActivity:

```java
private static final String BUG_API = "http://www.bug.hr/";

...

private void getRSSFeed() {
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BUG_API)
            .client(new OkHttpClient())
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build();
    RSSFeedAPI api = retrofit.create(RSSFeedAPI.class);
    // Getting the category from spinner and getting the RSSFeed
    Call<RSS> call = api.getRSSFeed(listCategory[spinnerCategory.getSelectedIndex()].toLowerCase());
    call.enqueue(this);
}

@Override
public void onResponse(Call<RSS> call, Response<RSS> response) {
    if(response.body() != null){
        List<Item> list = response.body().getChannel().getList();
        feedAdapter.refreshFeed(list);
    }
    else Toast.makeText(getApplicationContext(),
            "No results!", Toast.LENGTH_LONG).show();
    swipeContainer.setRefreshing(false);
}

@Override
public void onFailure(Call<RSS> call, Throwable t) {
    Log.i("Fail", t.getMessage());
}
```

* **Refreshing the feed**

User can easily refresh the feed by swiping down (`Pull-To-Refresh`).

To make that possible a `swipe layout` is used and the folowing code:

```XML
<android.support.v4.widget.SwipeRefreshLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:id="@+id/swipeContainer"
        tools:context="hr.ferit.slavenivic.bugsy.FeedActivity"
        android:background="@color/colorBg">

        <android.support.v7.widget.RecyclerView
            android:layout_width="match_parent"
            android:id="@+id/rv_feed"
            android:layout_height="wrap_content"
            android:layout_alignParentStart="true"
            android:layout_alignParentTop="true"/>

</android.support.v4.widget.SwipeRefreshLayout>
```

```java
private void setupSwipeToRefresh(){
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRSSFeed();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }
```

### Comment

* Newtorking part of the app is done using `Retrofit` and modeling the data of the XML response is done using `Simple XML`.

* Limited or no informations on [bug.hr API](http//www.bug.hr/rss).
